import React from 'react'
import Message from './Message'
class MessageList extends React.Component{
    constructor(props){
        super(props);
        this.state={
            data:this.props.list,
        }
    }
    render(){
        const {data}=this.state;
        return(
            <ul className="Chat-box">
                {data.map(el=>
                    <li><Message data={el}/></li>
                )}
            </ul>
        );
    }
}
export default MessageList;