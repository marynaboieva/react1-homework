import React from 'react';
import galaxy from './galaxy.svg'
import './Chat.css'
import MessageList from './MessageList';
import Input from './Input';
import Header from './Header';
class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: false,
    };
  }
  componentDidMount() {
    this.setState({ isLoading: true });
    fetch("https://api.myjson.com/bins/1hiqin")
      .then(response => response.json())
      .then(data => this.setState({ data: data, isLoading: false }));
  }
  onSendMessage = (message) => {
    const messages = this.state.data;
    message.id=messages[messages.length-1].id+1;
    messages.push(message);
    this.setState({data: messages})
  }
  render() {
    const { data, isLoading } = this.state;
    if (isLoading) {
      return <img src={galaxy} className="Chat-logo" alt="logo"/>;
    }

    return (
      <div id="App">
        <Header list={data}/>
        <MessageList list={data}/>
        <Input onSendMessage={this.onSendMessage}/>
      </div>
    );
  }
}

export default Chat;
