import React from 'react'
import './Message.css'
class Message extends React.Component{
    constructor(props){
        super(props);
        this.state=props.data;
    }
    render(){
        const data=this.state;
        if(data.user==="me"){
            return( <div key={data.id} className="message message--me">
            <header className="message-header">
                <p>{data.user}</p>
                <span>{data.created_at}</span>
            </header>
            <p className="message-text">{data.message}</p>
            <button className="message-edit">✎</button>
        </div>)
        }
       else return( <div key={data.id} className="message message--someone">
            <header className="message-header">
                <p>{data.user}</p>
                <span>{data.created_at}</span>
            </header>
            <img className="message-avatar" alt="avatar" src={data.avatar}/>
            <p className="message-text">{data.message}</p>
            <span  className="message-like"><input id={data.id} type="checkbox"/><label for={data.id}>★</label></span>
        </div>)
    }
}
export default Message;