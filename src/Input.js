import React from 'react'

class Input extends React.Component{
    constructor(props){
        super(props);
        this.state={
            id:"",
            user:"me",
            avatar:"",
            created_at:0,
            message:"",
            marked_read:false
        };
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleChange(e) {
        let time=new Date();
        time=time.getFullYear()+"-"+time.getMonth()+"-"+time.getDay()+" "+time.getHours()+":"+time.getMinutes()+":"+time.getSeconds();
        this.setState({
            id:"",
            user:"me",
            avatar:"",
            message: e.target.value,
            created_at:time,
            marked_read:false
        });
      }
      handleSubmit(e) {
        e.preventDefault()
        this.props.onSendMessage(this.state)
        this.setState({
            id:"",
            user:"me",
            avatar:"",
            created_at:0,
            message:"",
            marked_read:false
        })
      }
      render() {
        return (
          <form
            onSubmit={this.handleSubmit}
            className="send-message-form">
            <input
              onChange={this.handleChange}
              value={this.state.message}
              placeholder="Type your message and hit ENTER"
              type="text" />
          </form>
        )
      }
    }
export default Input;