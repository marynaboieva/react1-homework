import React from 'react'
import galaxy2 from './galaxy2.svg'

class Header extends React.Component{
    constructor(props){
        super(props);
        this.state={
            list:props.list,
            users:[...new Set(props.list.map(item => item.user))]
        };
    }
    render(){
        const {list,users}=this.state;
        return (<header className="Chat-header">
            <img src={galaxy2} alt="logo"/>
            <span>{users.length+1} users</span>
            <span>{list.length} messages</span>
        </header>)
    }
}
export default Header;